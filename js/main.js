(function () {
    const shadowDOM = document.createElement('div');
    shadowDOM.attachShadow({mode: 'open'});
    const searchPlagin = document.createElement('div');
    searchPlagin.className = 'searchPlagin';
    document.body.append(searchPlagin);

  
    const searchPlaginStyle = {
      'display': 'flex',
      'cursor': 'move',
      'flex-direction': 'column',
      'align-items': 'center',
      'border': '3px solid black',
      'background-color': 'white',
      'width': '400px',
      'height': '210px',
      'position': 'fixed',
      'border-radius': '3%',
      'top': '10%',
      'left': '60%',
      
    };
  
    
  
    searchPlagin.innerHTML = `
      <span style='position: absolute; 
      cursor: pointer; 
      top: 7px; 
      right: 7px;
      background-color:whitesmoke' 
      class='button__close'
      >
      
      |||


      </span>
      <h1 class='searchPlagin__title'>Look through elements</h1>
      <form>
        <input class='input__search' type='text' name='search' value=''>
        <input class='button__search' type='submit' name='submit' value='Search' style='cursor: pointer;'>
      </form>
      <h2 class='found__tag'></h2>
      <div class='button' style='width: 90%; display: flex; margin-bottom: 15px; margin-left:5px'>
        <button class='button__prev' style='margin-left:5px'>Previous</button>
        <button class='button__next' style='margin-left:5px'>Next</button>
        <button class='button__parent' style='margin-left:5px'>Parent</button>
        <button class='button__child' style='margin-left:5px'>Child</button>
      </div>`

      Object.assign(searchPlagin.style, searchPlaginStyle);



      
    const btns = searchPlagin.querySelectorAll('button');
    const btnClose = searchPlagin.querySelector('.button__close');
    const btnPrev = searchPlagin.querySelector('.button__prev');
    const btnNext = searchPlagin.querySelector('.button__next');
    const inputSearch = searchPlagin.querySelector('.input__search');
    const btnParent = searchPlagin.querySelector('.button__parent');
    const btnSearchFor = searchPlagin.querySelector('.button__search');
    const btnChild = searchPlagin.querySelector('.button__child');
   
    
    
  
  
  
  
    btns.forEach(el => {
      el.className = 'button__disabled';
      el.setAttribute('style', 'width: 25%; height: 30px; cursor: pointer;');
      el.setAttribute('disabled', 'disabled');
    });

    shadowDOM.appendChild(searchPlagin);
    shadowDOM.shadowRoot.appendChild(searchPlagin);
    document.body.prepend(shadowDOM);
  
    function handlerInput(ev) {
      inputSearch.setAttribute('value', `${ev.target.value}`)
    };
  
    let inputText;
    let inpTag;
  
    function onbtnSearchFor(ev) {
      ev.preventDefault()
      inputText = inputSearch.value;
      inpTag = inputText ? document.querySelectorAll(inputText)[0] : ''
      if (inpTag) {
        inpTag.style.border = '3px solid red';
        setButtons();
      };
    };
  
  
  
    function setButtons() {
      if (inpTag.firstElementChild) {
        btnChild.disabled = false;
      } else {
        btnChild.disabled = true;
      }
      if (inpTag.parentElement) {
        btnParent.disabled = false;
      } else {
        btnParent.disabled = true;
      }
      if (inpTag.previousElementSibling) {
        btnPrev.disabled = false;
      } else {
        btnPrev.disabled = true;
      }
      if (inpTag.nextElementSibling) {
        btnNext.disabled = false;
      } else {
        btnNext.disabled = true;
      }
    };
  
    let currentTag = null;
    let prevTag = null;
  
    function setProperties(props) {
      prevTag = inpTag;
      prevTag.style.border = 'none';
      currentTag = props;
      currentTag.style.border = '1px solid red';
      currentTag.scrollIntoView({block: 'center'});
      inpTag = currentTag;
      inputSearch.value = currentTag.tagName;
    }
  
  
    function onbtnNext() {
      if (inpTag.nextElementSibling) {
        setProperties(inpTag.nextElementSibling)
      }
      setButtons();
    };
  
    function onbtnPrev() {
      if (inpTag.previousElementSibling) {
        setProperties(inpTag.previousElementSibling)
      }
      setButtons();
    };
  
    function onbtnParent() {
      if (inpTag.parentElement) {
        setProperties(inpTag.parentElement)
      }
      setButtons();
    };
  
    function onbtnChild() {
      if (inpTag.children) {
        setProperties(inpTag.firstElementChild)
      }
      setButtons();
    };
  
    inputSearch.addEventListener('input', handlerInput);
    btnSearchFor.addEventListener('click', onbtnSearchFor);
    btnNext.addEventListener('click', onbtnNext);
    btnParent.addEventListener('click', onbtnParent);
    btnPrev.addEventListener('click', onbtnPrev);
    btnChild.addEventListener('click', onbtnChild);
    btnClose.addEventListener('click', onbtnClose);
  
    function onbtnClose() {
      inputSearch.removeEventListener('input', handlerInput);
      btnSearchFor.removeEventListener('click', onbtnSearchFor);
      btnNext.removeEventListener('click', onbtnNext);
      btnParent.removeEventListener('click', onbtnParent);
      btnPrev.removeEventListener('click', onbtnPrev);
      btnChild.removeEventListener('click', onbtnChild);
      shadowDOM.remove();
    } 
  
    function dragElement(elmnt) {
      let X = 0,
       Y = 0, 
       x = 0, 
       y = 0;
      elmnt.onmousedown = dragMouseDown;
  
      function dragMouseDown(e) { 
          x = e.clientX;
          y = e.clientY;
          document.onmouseup = closeDragElement; 
          document.onmousemove = elementDrag;
      }
  
      function elementDrag(e) { 
          X = x - e.clientX;
          Y = y - e.clientY;
          x = e.clientX;
          y = e.clientY; 
          elmnt.style.top = (elmnt.offsetTop - Y) + 'px';
          elmnt.style.left = (elmnt.offsetLeft - X) + 'px';
      }
  
      function closeDragElement() { 
          document.onmouseup = null;
          document.onmousemove = null;
      }
  }
  
  dragElement(searchPlagin);
  
  })()
  




  // javascript:!function(){const e=document.createElement("div");e.attachShadow({mode:"open"});const t=document.createElement("div");t.className="searchPlagin",document.body.append(t);t.innerHTML="\n      <span style='position: absolute; \n      cursor: pointer; \n      top: 7px; \n      right: 7px;\n      background-color:whitesmoke' \n      class='button__close'\n      >\n      \n      |||\n\n\n      </span>\n      <h1 class='searchPlagin__title'>Look through elements</h1>\n      <form>\n        <input class='input__search' type='text' name='search' value=''>\n        <input class='button__search' type='submit' name='submit' value='Search' style='cursor: pointer;'>\n      </form>\n      <h2 class='found__tag'></h2>\n      <div class='button' style='width: 90%; display: flex; margin-bottom: 15px; margin-left:5px'>\n        <button class='button__prev' style='margin-left:5px'>Previous</button>\n        <button class='button__next' style='margin-left:5px'>Next</button>\n        <button class='button__parent' style='margin-left:5px'>Parent</button>\n        <button class='button__child' style='margin-left:5px'>Child</button>\n      </div>",Object.assign(t.style,{display:"flex",cursor:"move","flex-direction":"column","align-items":"center",border:"3px solid black","background-color":"white",width:"400px",height:"210px",position:"fixed","border-radius":"3%",top:"10%",left:"60%"});const n=t.querySelectorAll("button"),l=t.querySelector(".button__close"),o=t.querySelector(".button__prev"),i=t.querySelector(".button__next"),s=t.querySelector(".input__search"),r=t.querySelector(".button__parent"),c=t.querySelector(".button__search"),u=t.querySelector(".button__child");function a(e){s.setAttribute("value",`${e.target.value}`)}let d,b;function p(e){e.preventDefault(),d=s.value,(b=d?document.querySelectorAll(d)[0]:"")&&(b.style.border="3px solid red",m())}function m(){b.firstElementChild?u.disabled=!1:u.disabled=!0,b.parentElement?r.disabled=!1:r.disabled=!0,b.previousElementSibling?o.disabled=!1:o.disabled=!0,b.nextElementSibling?i.disabled=!1:i.disabled=!0}n.forEach(e=>{e.className="button__disabled",e.setAttribute("style","width: 25%; height: 30px; cursor: pointer;"),e.setAttribute("disabled","disabled")}),e.appendChild(t),e.shadowRoot.appendChild(t),document.body.prepend(e);let v=null,f=null;function h(e){(f=b).style.border="none",(v=e).style.border="1px solid red",v.scrollIntoView({block:"center"}),b=v,s.value=v.tagName}function _(){b.nextElementSibling&&h(b.nextElementSibling),m()}function y(){b.previousElementSibling&&h(b.previousElementSibling),m()}function x(){b.parentElement&&h(b.parentElement),m()}function E(){b.children&&h(b.firstElementChild),m()}s.addEventListener("input",a),c.addEventListener("click",p),i.addEventListener("click",_),r.addEventListener("click",x),o.addEventListener("click",y),u.addEventListener("click",E),l.addEventListener("click",function(){s.removeEventListener("input",a),c.removeEventListener("click",p),i.removeEventListener("click",_),r.removeEventListener("click",x),o.removeEventListener("click",y),u.removeEventListener("click",E),e.remove()}),function(e){let t=0,n=0,l=0,o=0;function i(i){t=l-i.clientX,n=o-i.clientY,l=i.clientX,o=i.clientY,e.style.top=e.offsetTop-n+"px",e.style.left=e.offsetLeft-t+"px"}function s(){document.onmouseup=null,document.onmousemove=null}e.onmousedown=function(e){l=e.clientX,o=e.clientY,document.onmouseup=s,document.onmousemove=i}}(t)}();